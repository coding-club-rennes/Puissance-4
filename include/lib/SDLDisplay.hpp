/*
** SDLDisplay.hpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 12:06:35 2015 Alexis Bertholom
// Last update Mon Feb 22 22:47:46 2016 baille_l
*/

#ifndef SDLDISPLAY_HPP_
# define SDLDISPLAY_HPP_

# include <string>
# include <SDL2/SDL.h>
# include "SDLContext.hpp"
# include "Display.hpp"
# include "Image.hpp"

class		SDLDisplay : protected SDLContext, public Display
{
public:
  SDLDisplay(std::string const& winTitle, Uint winW, Uint winH,
	     Uint winX = SDL_WINDOWPOS_UNDEFINED,
	     Uint winY = SDL_WINDOWPOS_UNDEFINED);
  ~SDLDisplay();

public:
  void		putPixel(Uint x, Uint y, Color color);
  void		putRect(Uint x, Uint y, Uint w, Uint h, Color color);
  void		putImage(Uint x, Uint y, Image& img,
			 Uint w = Image::max, Uint h = Image::max);
  void		clearScreen();
  void		fillScreen(Color color);
  void          putGrid(int tab[4][4], int x, int y);
  void		refreshScreen();
  Uint		getWinW() const;
  Uint		getWinH() const;

private:
  Uint		_winW;
  Uint		_winH;
  SDL_Window*	_window;
  Image*	_screen;
};

#endif
